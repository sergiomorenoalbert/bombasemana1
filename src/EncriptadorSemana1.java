public class EncriptadorSemana1 {
	//Atributos
	private String mensajeInicial = "";
	private String mensajeFinal = "";
	private String mensajeFinalReverse = "";

	//Constructor
	public EncriptadorSemana1() {
		this.mensajeInicial = new String("");
		this.mensajeFinal = new String("");
		this.mensajeFinalReverse = new String ("");
	}
	//Getter para el mensaje inicial
	public String getMensaje() {
		return mensajeInicial;
	}
	//Getter para el mensaje final
	public String getMensajeFinal(String mensajeFinal) {
		return mensajeFinal;
	}
	//Setter para mensaje Inicial
	public void setMensajeInicial(String mensajeInicial) {
		this.mensajeInicial = mensajeInicial;
	}
	//Setter para mensaje Final
	public void setMensajeFinal() {
		this.mensajeFinal = mensajeFinal;
	}
	public String encriptador() {
		int length = mensajeInicial.length();
		//Reemplazamos caracteres
		for (int i=0;i<mensajeInicial.length();i++){
			mensajeInicial = mensajeInicial.replace('E','S').replace('A','E').replace('T','R').replace('E','G').replace('O','I').replace('L','O');
		}
		//Invertimos la cadena
	    for ( int i = length - 1 ; i >= 0 ; i-- ) {
	    	mensajeFinalReverse += mensajeInicial.charAt(i);
	    }
		//Reemplazamos los espacios
	    mensajeFinalReverse = mensajeFinalReverse.replaceAll("\\s+","-OK-");
		//Devolvemos el valor
		return mensajeFinalReverse;
	}

}
